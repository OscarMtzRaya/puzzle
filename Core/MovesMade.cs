﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Core
{
    public class MovesMade : HashSet<Matrix>
    {
        private static HashSet<Matrix> MovesMadeHashSet = new HashSet<Matrix>(new MatrixComparer());
        
        public static bool AddToSet(Matrix matrix)
        {
            return MovesMadeHashSet.Add(matrix);
        }

        public static void ResetHashSet()
        {
            MovesMadeHashSet = new HashSet<Matrix>(new MatrixComparer());
        }

        public static void PrintMovesMadeHashSet()
        {
            Console.WriteLine("\nHashSet\n");
            List<Matrix> list = MovesMadeHashSet.ToList();
            list.ForEach(matrix => { 
                Matrix.PrintMatrix(matrix);
            });
        }

        public static int NumberOfMoves()
        {
            return MovesMadeHashSet.Count;
        }
    }
}
