﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Core
{
    public class RandomMatrixGenerator
    {
        public static IEnumerable<int> GenerateRandom()
        {            
            Random random = new Random();            
            var numbers = Enumerable.Range(0, 9);
            var shuffle = numbers.OrderBy(a => random.NextDouble());
            Matrix matrix = new Matrix(shuffle);
            MatrixSingleton.matrix = new Matrix
            {
                InnerMatrix = matrix.InnerMatrix.Clone() as int[,]
            };
            return shuffle;
        }
    }
}
