﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public class ParametersSingleton
    {
        //1: Positions
        //2: Distance
        //3: Both
        public static int Fn { get; set; }

        //1: Euclidian
        //2: Manhattan
        public static int Heuristic { get; set; }
    }
}
