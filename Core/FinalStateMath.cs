﻿namespace Core
{
    public class FinalStateMath
    {
        private static Matrix FinalState{ get; set;}        
        
        public static double CalculateFn(Matrix matrix)
        {
            Matrix LocalMatrix = new Matrix
            {
                InnerMatrix = matrix.InnerMatrix.Clone() as int[,]
            };

            FinalState = FillFinalState();

            switch(ParametersSingleton.Fn)
            {
                case 1:
                    return Position(matrix);

                case 2:
                    return Distance(matrix);

                case 3:
                    return PositionAndDistance(matrix);

                default:
                    return 0;
            }
        }

        private static double Position(Matrix matrix)
        {
            double outOfPosition = 0;

            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    if (matrix.InnerMatrix[i,j] != FinalState.InnerMatrix[i,j])
                    {
                        outOfPosition++;
                    }
                }
            }            
            return outOfPosition;
        }

        private static int Distance(Matrix matrix)
        {
            return 0;
        }

        private static int PositionAndDistance(Matrix matrix)
        {
            return 0;
        }

        private static Matrix FillFinalState()
        {
            int counter = 1;
            Matrix matrix = new Matrix();
            
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    matrix.InnerMatrix[i,j] = counter;
                    counter++;
                }
            }

            matrix.InnerMatrix[2,2] = 0;
            return matrix;
        }

        public static bool IsSolved(Matrix matrix)
        {
            return matrix.Fn == 0;
        }
    }
}
