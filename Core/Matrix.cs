﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Core
{
    public class Matrix
    {
        public int[,] InnerMatrix { get; set; }
        public double Fn { get; set; }
        public int HashCode { get; set; }
        public string Key {get; set;}

        public Matrix()
        {
            InnerMatrix = new int[3,3];
        }

        public Matrix(IEnumerable<int> list)
        {                        
            InnerMatrix = new int[3,3];
            List<int> aux = list.ToList();
            int counter = 0;
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {                                        
                    InnerMatrix[i,j] =  aux[counter];
                    counter++;
                }
            }
        }
        
        public static void PrintMatrix(Matrix matrix)
        {
            for (int i = 0; i < 3; i++)
            {
                Console.WriteLine();
                for (int j = 0; j < 3; j++)
                {
                    Console.Write(matrix.InnerMatrix[i,j] + " ");                    
                }
            }
            Console.WriteLine("F(n) : " + matrix.Fn);
            Console.WriteLine("HashCode: " + matrix.HashCode);
            Console.WriteLine("°°°°°°°°°°°°°°°°°°°°°°°");
            Console.WriteLine("\n");
        }

        public void BuildHashCode()
        {
            string str = "";
            for (int i = 0; i < 3; i++)
            {                
                for (int j = 0; j < 3; j++)
                {
                    str += InnerMatrix[i,j].ToString();
                }
            }
            HashCode = int.Parse(str);
            Key = str;
        }        
    }
}