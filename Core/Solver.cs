﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;

namespace Core
{
    public class Solver
    {        
        private Matrix Matrix { get; set; }                

        public Solver(Matrix matrix)
        {
            Solve(matrix);            
        }

        private void Solve(Matrix matrixIn)
        {            
            List<Matrix> list = Explore(matrixIn);
            Matrix matrix  = new Matrix();
            
            foreach (Matrix matrixInList in list)
            {
                if (MovesMade.AddToSet(matrixInList))
                {
                    matrix = matrixInList;
                    break;
                }
            }
            
            if (!(matrix.Key == null)  )
            {
                //System.Console.WriteLine("\nLocalMatrix in Solve\n");
                //Matrix.PrintMatrix(matrix);
                MovesMade.AddToSet(matrix);                

                if (!FinalStateMath.IsSolved(matrix))
                {
                    Solve(matrix);
                } else
                {                    
                    MessageBox.Show("Solucionado\nNúmero de movimientos: " + MovesMade.NumberOfMoves());
                    System.Console.WriteLine("+++ Lista de movimientos aceptados +++");
                    MovesMade.PrintMovesMadeHashSet();
                }
            } else
            {
                MessageBox.Show("Solución no encontrada\nNúmero de movimientos: " + MovesMade.NumberOfMoves()
                    + "\nVerifique la consola");
                System.Console.WriteLine("+++ Lista de movimientos aceptados +++");
                MovesMade.PrintMovesMadeHashSet();
            }                        
        }

        private List<Matrix> Explore(Matrix matrix)
        {
            Position zero = FindZero(matrix);
            List<Position> possibleMoves = PossibleMoves.GetPossibleMoves(zero);
            List<Matrix> possibleStates = GetPossibleStates(possibleMoves, zero, matrix);
            
            possibleStates.ForEach(localMatrix => {
                localMatrix.Fn = FinalStateMath.CalculateFn(localMatrix);
                localMatrix.BuildHashCode();
            });

            possibleStates = possibleStates.OrderBy(x => x.Fn).ThenBy(x => x.Key).ToList();
            //PrintMatrixList(possibleStates);

            return possibleStates;
        }

        private Position FindZero(Matrix matrix)
        {
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    if (matrix.InnerMatrix[i,j] == 0)
                    {
                        return new Position(i, j);
                    }
                }
            }
            return null;
        }

        private List<Matrix> GetPossibleStates(List<Position> possibleMoves, Position zero, Matrix matrixIn)
        {
            //System.Console.WriteLine("\nPossible states  °°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°");
            List<Matrix> list = new List<Matrix>();
            int carry, carryZero;

            possibleMoves.ForEach( position => 
            {
                Matrix matrix = new Matrix
                {
                    InnerMatrix = matrixIn.InnerMatrix.Clone() as int[,]
                };
                carry = matrix.InnerMatrix[position.i, position.j];
                carryZero = matrix.InnerMatrix[zero.i, zero.j];
                matrix.InnerMatrix[zero.i, zero.j] = carry;
                matrix.InnerMatrix[position.i, position.j] = carryZero;

                list.Add(matrix);
            });            

            return list;
        }

        private void PrintMatrixList(List<Matrix> list)
        {
            list.ForEach(matrix => {
                Matrix.PrintMatrix(matrix);
                });
        }
    }
}
