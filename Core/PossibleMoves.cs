﻿using System.Collections.Generic;

namespace Core
{
    public class PossibleMoves
    {
        public static List<Position> GetPossibleMoves(Position position)
        {
            List<Position> list = new List<Position>();
            string pos = position.i.ToString() + position.j.ToString();

            switch(pos)
            {
                case "00":                    
                    list.Add(new Position(0,1));
                    list.Add(new Position(1,0));
                    return list;
                
                case "01":
                    list.Add(new Position(0,0));
                    list.Add(new Position(0,2));
                    list.Add(new Position(1,1));
                    return list;
                
                case "02":
                    list.Add(new Position(0,1));
                    list.Add(new Position(1,2));
                    return list;

                case "10":
                    list.Add(new Position(0,0));
                    list.Add(new Position(1,1));
                    list.Add(new Position(2,0));
                    return list;

                case "11":
                    list.Add(new Position(0,1));
                    list.Add(new Position(1,0));
                    list.Add(new Position(1,2));
                    list.Add(new Position(2,1));
                    return list;

                case "12":
                    list.Add(new Position(0,2));
                    list.Add(new Position(2,2));
                    list.Add(new Position(1,1));
                    return list;

                case "20":
                    list.Add(new Position(2,1));
                    list.Add(new Position(1,0));
                    return list;

                case "21":
                    list.Add(new Position(2,0));
                    list.Add(new Position(1,1));
                    list.Add(new Position(2,2));
                    return list;

                case "22":
                    list.Add(new Position(2,1));
                    list.Add(new Position(1,2));
                    return list;
                
                default:
                    return null;
            }
        }                    
    }
}
