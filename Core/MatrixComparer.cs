﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public class MatrixComparer : IEqualityComparer<Matrix>
    {
        public bool Equals(Matrix x, Matrix y)
        {
            return (x.Fn == y.Fn);            
        }

        public int GetHashCode(Matrix obj)
        {
            Matrix matrix = obj as Matrix;
            return matrix.HashCode;            
        }        
    }
}
