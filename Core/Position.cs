﻿namespace Core
{
    public class Position
    {
        public int i { get; set; }
        public int j { get; set; }

        public Position(int i, int j)
        {
            this.i = i;
            this.j = j;
        }

        public bool Equals(Position pos1, Position pos2)
        {
            return(pos1.i == pos2.i && pos1.j == pos2.j);                            
        }
    }
}
