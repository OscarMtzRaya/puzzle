﻿using System.Collections.Generic;
using System.Windows.Controls;

namespace Core
{
    public class Exchangable
    {


        private static List<string> GetAvailableExchanges(string buttonName)
        {
            List<string> list = new List<string>();

            switch(buttonName)
            {
                case "button1":
                    list.Add("button2");
                    list.Add("button4");
                    return list;

                case "button2":
                    list.Add("button1");
                    list.Add("button3");
                    list.Add("button5");
                    return list;

                case "button3":
                    list.Add("button2");
                    list.Add("button6");
                    return list;

                case "button4":
                    list.Add("button1");
                    list.Add("button5");
                    list.Add("button7");
                    return list;

                case "button5":
                    list.Add("button2");
                    list.Add("button4");
                    list.Add("button6");
                    list.Add("button8");
                    return list;

                case "button6":
                    list.Add("button3");
                    list.Add("button5");
                    list.Add("button0");
                    return list;

                case "button7":
                    list.Add("button4");
                    list.Add("button8");
                    return list;

                case "button8":
                    list.Add("button7");
                    list.Add("button5");
                    list.Add("button0");
                    return list;

                case "button0":
                    list.Add("button6");
                    list.Add("button8");
                    return list;

                default:
                    return null;
            }
            
        }

        public static bool IsMoveValid(string buttonName, string destinyButton)
        {
            bool Valid = false;        
            List<string> availableButtons = GetAvailableExchanges(buttonName);
            availableButtons.ForEach(name => {
                if (name.Equals(destinyButton))
                {
                    Valid = true;                    
                } 
            });

            return Valid;
        }
    }
}
