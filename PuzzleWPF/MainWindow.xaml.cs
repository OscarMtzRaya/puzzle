﻿using Core;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using Matrix = Core.Matrix;

namespace PuzzleWPF
{
    public partial class MainWindow : Window
    {
        private int Number1;
        private int Number2;
        private Button FirstButton;
        private bool Clicked = false;
        private Button[] ButtonArray = new Button[9];
        private string CurrentState { get; set; }
        private string FinalState { get; set; }
        private Thread thread;

        public MainWindow()
        {
            InitializeComponent();
        }
        
        private void WindowLoaded(object sender, RoutedEventArgs e)
        {
            comboBoxFn.SelectedIndex = 0;
            comboBoxHeuristica.SelectedIndex = 0;
            BuildButtonsArray();
            BuildFinalState();
            FillWithRandom();
            ParametersSingleton.Fn = 1;            
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;
            string Content = button.Content.ToString();
            Object objContent = button.Content;

            if (!Clicked)
            {
                Int32.TryParse(Content, out Number1);
                FirstButton = button;                
                Clicked = true;
            } else
            {
                if (FirstButton.Content.ToString().Equals("0") || button.Content.ToString().Equals("0"))                    
                {
                    if (Exchangable.IsMoveValid(FirstButton.Name, button.Name))
                    {
                        Int32.TryParse(Content, out Number2);
                        FirstButton.Content = objContent;
                        button.Content = Number1;
                        BuildCurrentState();
                        if (IsSolved())
                        {
                            MessageBox.Show("¡Resuelto!");
                        }
                    }
                }
                Clicked = false;
            }
        }       

        private void BuildFinalState()
        {
            FinalState = "123456780";
        }                    

        private bool IsSolved()
        {
            return CurrentState.Equals(FinalState);            
        }        

        private void FillWithRandom()
        {
            IEnumerable<int> list = RandomMatrixGenerator.GenerateRandom();
            int counter = 0;
            foreach (int number in list)
            {
                ButtonArray[counter].Content = number;
                counter++;
            }
        }

        private void BtnGenerar_Click(object sender, RoutedEventArgs e)
        {
            FillWithRandom();
        }

        private void BuildButtonsArray()
        {
            ButtonArray[0] = button1;
            ButtonArray[1] = button2;
            ButtonArray[2] = button3;
            ButtonArray[3] = button4;
            ButtonArray[4] = button5;
            ButtonArray[5] = button6;
            ButtonArray[6] = button7;
            ButtonArray[7] = button8;
            ButtonArray[8] = button0;            
        }        

        private void BtnCheat_Click(object sender, RoutedEventArgs e)
        {
            if (thread != null)
            {                
                thread.Abort();
            }
            int counter = 0;
            List<int> list = new List<int>
            {
                2,
                5,
                8,
                3,
                6,
                0,
                4,
                7,
                1
            };

            foreach (int number in list)
            {
                ButtonArray[counter].Content = number;
                counter++;
            }
        }

        private Matrix MakeMatrix()
        {
            Matrix matrix = new Matrix();
            int counter  = 0;
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {                    
                    matrix.InnerMatrix[i,j] = int.Parse(ButtonArray[counter].Content.ToString());
                    counter++;
                }
            }
            return matrix;
        }

        private void BtnResolver_Click(object sender, RoutedEventArgs e)
        {
            MovesMade.ResetHashSet();
            Matrix matrix = MakeMatrix();
            ParametersSingleton.Fn = (comboBoxFn.SelectedIndex+1);
            thread = new Thread(start =>
            {
                Solver solver = new Solver(matrix);
            });
            thread.Start();
        }

         private void BuildCurrentState()
        {
            string currentState =
                button1.Content.ToString() + 
                button2.Content.ToString() + 
                button3.Content.ToString() + 
                button4.Content.ToString() + 
                button5.Content.ToString() + 
                button6.Content.ToString() + 
                button7.Content.ToString() + 
                button8.Content.ToString() + 
                button0.Content.ToString();

            CurrentState = currentState;            
        }
    }
}
